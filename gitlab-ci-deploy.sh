#!/usr/bin/env bash
set -euxo pipefail

mvn deploy -X -s ci_settings.xml