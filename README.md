# Java coding challenge

## [Click here to open the project in a workspace on Gitpod.io](https://gitpod.io/#https://gitlab.com/interview-wilj/countries-api)

![Gitpod.io workspace example](docs/gitpod-screenshot.png)

## Local development instructions

### Populating the database

The database population scripts are in `src/main/sql` and are comprised of a SQL file containing the schema definition,
and a CSV file containing the data to load.

Any of the following 3 methods for launching the database should result in a populated PostgreSQL database listening on localhost:5432.

**Note:** Commands must be run from the project directory.

#### Docker
```shell
    docker run \
      -e "POSTGRES_USER=challenge" \
      -e "POSTGRES_PASSWORD=" \
      -e "POSTGRES_HOST_AUTH_METHOD=trust" \
      -v "$(pwd)/src/main/sql:/docker-entrypoint-initdb.d:ro" \
      -p "127.0.0.1:5432:5432" \
        docker.io/library/postgres:14.2
```

#### docker-compose
```shell
  docker-compose -f postgresql.yml up
```

#### Vagrant + Virtualbox
```shell
  vagrant up  
```

---

### Building and running the application

#### IntelliJ IDEA

The project includes run configurations for IntelliJ IDEA:
* challenge-test - runs unit tests and generates API documentation
* challenge-package - builds the executable JAR file
* challenge-run - runs the application

#### Maven

```shell
mvn clean install spring-boot:repackage
cd target
java -Dspring.datasource.url=jdbc:postgresql://localhost:5432/challenge -Dspring.datasource.username=challenge -jar challenge-0.0.1-SNAPSHOT.jar
```

---

## Implementation notes


1.  Source code for your solution to the above requests
    * Spring Boot + JPA implementation
        * https://gitlab.com/interview-wilj/countries-api 
    * React + Typescript + Tailwind CSS UI to demonstrate API usage:
        * https://gitlab.com/interview-wilj/countries-ui
2.  Schema of the database defined to hold the above data
    * https://gitlab.com/interview-wilj/countries-api/-/blob/main/src/main/sql/0001-create-schema.sql
    * https://gitlab.com/interview-wilj/countries-api/-/blob/main/src/main/sql/data.csv
3.  The mechanism used to populate the initial values in the database (hint: don’t hard code these entries)
    * Since this task is big-data specific, I used an ETL approach to load the data.
        1. I copied the table from the Word document into an Excel spreadsheet, and saved as CSV.
        2. The schema population script uses the PostgreSQL COPY feature to read the sparse CSV into a temporary table.
            * Using a temporary table reduces Write-Ahead Logging (WAL) while the data is initially process.
            * The COPY statement will read/write the data at near-disk speeds, eliminating SQL overhead for the initial load.
        3. A PL/pgSQL block traverses the temporary table, populating the missing country codes.
            * The updates are accomplished with a single full table scan to read the data, and primary key access for updates.
        4. The target tables are then populated from the temporary table by INSERT...SELECT statements.
            * Reduces index churn by bulk SELECT -> TRANSFORM -> INSERTing the rows.
    * The anonymous PL/pgSQL blocks in the schema creation script could be wrapped in a stored procedure or Java service for reuse when uploading other files, but I did not implement it for this exercise.
4.  WAR file or Spring Boot Jar we can run in tomcat to test your code
    * The Spring Boot jar file can be found at:
        * https://gitlab.com/interview-wilj/countries-api/-/package_files/47788829/download
5. Additional dependencies/features:
    * Maven enforcer plugin:
        * Requires dependency convergence.
        * Bans log4j dependencies.
    * Generated OpenAPI schema for generating typesafe clients:
        * https://gitlab.com/interview-wilj/countries-ui/-/blob/main/package.json#L28
        * https://gitlab.com/interview-wilj/countries-ui/-/blob/main/src/generated/services/ApiControllerService.ts
    * Spring REST Docs with example usage recorded from unit tests:
        * https://gitlab.com/interview-wilj/countries-api/-/blob/main/src/test/java/net/wilj/challenge/ChallengeApplicationTests.java#L35
    * Non-JPA implementation, just for example:
        * https://gitlab.com/interview-wilj/countries-api-jdbc

---

## Original challenge requirements

Create a database of your desired flavor that can store simple data based on the following fictional country and state information:

<div id="data">

| Country     | Country Code  | Currency  | State            | State Code  | State Population  |
|-------------|---------------|-----------|------------------|-------------|-------------------|
| Big Land    | BLD           | CHK       | Left Province    | LEF         | 10,100            |
|             |               |           | Right Province   | RIG         | 778,030           |
|             |               |           | Topside          | TSD         | 2,200,340         |
|             |               |           | Center Province  | CTR         | 1,340,922         |
| Mordor      | MDR           | GUL       | Udun             | UDN         | 2,000,110         |
|             |               |           | Gorgoroth        | GOR         | 3,120,900         |
|             |               |           | Nurn             | NRN         | 1,100,000         |
|             |               |           | Kand             | KND         | 2,500,000         |
| Numberland  | NUM           | DIG       | One              | ONE         | 1,1500,00         |
|             |               |           | Two              | TWO         | 25,320,000        |
|             |               |           | Three            | TRE         | 3,100,00          |
|             |               |           | Four             | FOR         | 400,000           |

</div>

Create the following web services to access this data:

1.  getAllCountryPopulations()

    - GET method that returns all of the countries with the total populations of each country as a JSON object keyed on country code

1.  getAllCountryCurrencies()

    - GET method that returns all of the countries with the currency of each country as a JSON object keyed on country code

1.  validateState(country\_code, state\_code)

    - GET method that returns a boolean indicating whether or not the supplied county code and state code combination are valid

1.  addState(country\_cd, state\_cd, state\_name, state\_population)

    - PUT method that will add the supplied state information to the database above

Please implement your solution in Java and send us the following:

1.  Source code for your solution to the above requests
2.  Schema of the database defined to hold the above data
3.  The mechanism used to populate the initial values in the database (hint: don’t hard code these entries)
4.  WAR file or Spring Boot Jar we can run in tomcat to test your code

If you have any questions, please use your best judgment and document any assumptions you have made.