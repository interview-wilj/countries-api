FROM gitpod/workspace-full

USER gitpod

RUN bash -c ". /home/gitpod/.sdkman/bin/sdkman-init.sh && \
    sdk install java 17.0.3-ms && \
    sdk default java 17.0.3-ms"

RUN wget https://github.com/neovim/neovim/releases/download/v0.7.0/nvim-linux64.deb \
    && sudo dpkg -i nvim-linux64.deb \
    && rm nvim-linux64.deb
