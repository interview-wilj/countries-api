package net.wilj.challenge.jpa;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "challenge", name = "countries")
@Data
@NoArgsConstructor
public class Countries {
    @Id
    private String code;
    private String name;
    private String currency;
}
