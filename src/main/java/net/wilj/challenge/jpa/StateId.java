package net.wilj.challenge.jpa;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class StateId implements Serializable {
    private String countryCode;
    private String code;

    public StateId(String countryCode, String code) {
        this.countryCode = countryCode;
        this.code = code;
    }
}
