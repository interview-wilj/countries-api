package net.wilj.challenge.jpa;

import net.wilj.challenge.dto.Currency;
import net.wilj.challenge.dto.Population;
import net.wilj.challenge.dto.State;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class CustomCountriesRepositoryImpl implements CustomCountriesRepository {

    private final NamedParameterJdbcTemplate jdbc;

    public CustomCountriesRepositoryImpl(NamedParameterJdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    /**
     * @return all of the countries with the currency of each country.
     */
    public List<Currency> getAllCountryCurrencies() {
        return jdbc.query("""
                    select
                        code,
                        currency
                    from countries
                """, Collections.emptyMap(), new BeanPropertyRowMapper<>(Currency.class));
    }

    /**
     * @return all of the countries with the total populations.
     */
    public List<Population> getAllCountryPopulations() {
        return jdbc.query("""
                    select
                        country_code code,
                        sum(population) population
                    from states
                    group by country_code
                """, Collections.emptyMap(), new BeanPropertyRowMapper<>(Population.class));
    }

    /**
     * @return a boolean indicating whether or not the supplied county code and state code combination are valid.
     */
    public boolean validateState(String countryCode, String stateCode) {
        var params = Map.of("countryCode", countryCode, "stateCode", stateCode);
        return jdbc.queryForObject("""
                    select
                        case
                            when count(s.*) > 0 then true
                            else false
                        end
                    from states s
                    where s.country_code = :countryCode
                        and s.code = :stateCode
                """, params, Boolean.class);
    }


    /**
     * @return a boolean indicating whether or not the supplied county code and state code combination are valid.
     */
    public State addState(
            String countryCode,
            String code,
            String name,
            Integer population
    ) {
        var params = Map.of(
                "countryCode", countryCode,
                "code", code,
                "name", name,
                "population", population);

        return jdbc.queryForObject("""
                    insert into states (country_code, code, name, population)
                    values (upper(:countryCode), upper(:code), :name, :population)
                        on conflict (country_code, code)
                        do update set
                            name = :name,
                            population = :population
                        returning country_code, code as state_code, name as state_name, population
                """, params, new BeanPropertyRowMapper<>(State.class));
    }

}
