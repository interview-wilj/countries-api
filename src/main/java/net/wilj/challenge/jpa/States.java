package net.wilj.challenge.jpa;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(schema = "challenge", name = "states")
@IdClass(StateId.class)
@Data
@NoArgsConstructor
public class States {
    @Id
    private String countryCode;
    @Id
    private String code;
    private String name;
    private long population;
}
