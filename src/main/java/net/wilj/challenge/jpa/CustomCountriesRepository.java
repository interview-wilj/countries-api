package net.wilj.challenge.jpa;

import net.wilj.challenge.dto.Currency;
import net.wilj.challenge.dto.Population;
import net.wilj.challenge.dto.State;

import java.util.List;

public interface CustomCountriesRepository {
    /**
     * @return all of the countries with the currency of each country.
     */
    List<Currency> getAllCountryCurrencies();

    /**
     * @return all of the countries with the total populations.
     */
    List<Population> getAllCountryPopulations();

    /**
     * @return a boolean indicating whether or not the supplied county code and state code combination are valid.
     */
    boolean validateState(String countryCode, String stateCode);

    /**
     * @return a boolean indicating whether or not the supplied county code and state code combination are valid.
     */
    State addState(
            String countryCode,
            String code,
            String name,
            Integer population
    );


}
