package net.wilj.challenge.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface StatesRepository extends PagingAndSortingRepository<States, StateId> {

}