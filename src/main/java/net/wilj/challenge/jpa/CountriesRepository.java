package net.wilj.challenge.jpa;

import org.springframework.data.repository.CrudRepository;

public interface CountriesRepository extends CrudRepository<Countries, String>, CustomCountriesRepository {

}