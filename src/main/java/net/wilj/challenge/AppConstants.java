package net.wilj.challenge;

public interface AppConstants {
    String DEFAULT_PAGE_NUMBER = "0";
    String DEFAULT_PAGE_SIZE = "10";
    String DEFAULT_SORT_BY = "name";
    String DEFAULT_SORT_DIRECTION = "asc";
}
