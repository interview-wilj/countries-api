package net.wilj.challenge.dto;

import lombok.Data;

@Data
public class Population {
    private String code;
    private Integer population;
}
