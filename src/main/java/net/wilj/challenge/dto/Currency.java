package net.wilj.challenge.dto;

import lombok.Data;

@Data
public class Currency {
    private String code;
    private String currency;
}

