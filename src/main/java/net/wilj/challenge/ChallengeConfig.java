package net.wilj.challenge;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

@Configuration
public class ChallengeConfig implements WebMvcConfigurer {

    final String[] API_DOCS_PATHS = {"/", "/apidocs", "/apidocs/"};
    final String API_DOCS_VIEW = "forward:/apidocs/index.html";

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        Arrays.stream(API_DOCS_PATHS).forEach(path -> registry.addViewController(path).setViewName(API_DOCS_VIEW));
    }
}