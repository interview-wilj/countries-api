package net.wilj.challenge;

import net.wilj.challenge.dto.State;
import net.wilj.challenge.jpa.CountriesRepository;
import net.wilj.challenge.jpa.States;
import net.wilj.challenge.jpa.StatesRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.TreeMap;

@RestController
@RequestMapping("/api/countries")
public class ApiController {

    final CountriesRepository countries;
    final StatesRepository states;

    public ApiController(final CountriesRepository countries, final StatesRepository states) {
        this.countries = countries;
        this.states = states;
    }

    @GetMapping("/populations")
    public ResponseEntity<?> getAllCountryPopulations() {
        Map<String, Integer> result = new TreeMap<>();
        countries.getAllCountryPopulations().forEach(c -> result.put(c.getCode(), c.getPopulation()));
        return ResponseEntity.ok(result);
    }

    @GetMapping("/currencies")
    public ResponseEntity<?> getAllCountryCurrencies() {
        Map<String, String> result = new TreeMap<>();
        countries.getAllCountryCurrencies().forEach(c -> result.put(c.getCode(), c.getCurrency()));
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{countryCode}/states/{stateCode}/validate")
    public boolean validateState(@PathVariable("countryCode") String countryCode, @PathVariable("stateCode") String stateCode) {
        return countries.validateState(countryCode, stateCode);
    }

    @PutMapping("/{countryCode}/states/{stateCode}")
    public State addState(@PathVariable("countryCode") String countryCode, @PathVariable("stateCode") String stateCode, @Valid @RequestBody State state) {
        if (countryCode.equalsIgnoreCase(state.getCountryCode()) && stateCode.equalsIgnoreCase(state.getStateCode())) {
            return countries.addState(state.getCountryCode(), state.getStateCode(), state.getStateName(), state.getPopulation());
        } else {
            throw new IllegalArgumentException("Path parameters must match request body.");
        }
    }

    // extra credit, not in the requirements but needed in an associated UI project
    @GetMapping("/states")
    public Page<States> allStates(
            @RequestParam(value = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pageNo,
            @RequestParam(value = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_BY, required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDir
    ) {
        var sort = "asc".equalsIgnoreCase(sortDir) ? Sort.Order.asc(sortBy) : Sort.Order.desc(sortBy);
        Pageable p = PageRequest.of(pageNo, pageSize, Sort.by(sort));
        return states.findAll(p);
    }


}