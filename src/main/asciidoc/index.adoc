= Countries API documentation

== addState

.request
include::{snippets}/addState/http-request.adoc[]

.response
include::{snippets}/addState/http-response.adoc[]

.curl
include::{snippets}/addState/curl-request.adoc[]


== getAllCountryCurrencies

.request
include::{snippets}/getAllCountryCurrencies/http-request.adoc[]

.response
include::{snippets}/getAllCountryCurrencies/http-response.adoc[]

.curl
include::{snippets}/getAllCountryCurrencies/curl-request.adoc[]


== getAllCountryPopulations

.request
include::{snippets}/getAllCountryPopulations/http-request.adoc[]

.response
include::{snippets}/getAllCountryPopulations/http-response.adoc[]

.curl
include::{snippets}/getAllCountryPopulations/curl-request.adoc[]


== validateState

.request
include::{snippets}/validateState/http-request.adoc[]

.response
include::{snippets}/validateState/http-response.adoc[]

.curl
include::{snippets}/validateState/curl-request.adoc[]




