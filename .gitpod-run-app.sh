#!/usr/bin/env bash
set -euxo pipefail

DB=challenge

runQuery() {
    docker-compose -f postgresql.yml exec -T postgresql psql -h localhost -U $DB -X -A -t -c "$*"
}

databaseIsUp() {
    runQuery "select 'database' || 'available'" | grep -q databaseavailable
}

# wait for the database to start
RETRIES=30
until databaseIsUp > /dev/null 2>&1 || [ $RETRIES -eq 0 ]; do
    echo "Waiting for postgresql server, $((RETRIES--)) remaining attempts..."
    sleep 5
done

# build, test, and generate docs
./mvnw clean install spring-boot:repackage
 
# run app
./mvnw spring-boot:run